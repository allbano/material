# Aula 08 - Árvores Binárias de Busca

## Material

1. [Árvores Binárias de Busca](https://gitlab.com/ds143-alexkutzke/material/raw/main/08/00_abb/abb.pdf);
2. [Implementação simples](https://gitlab.com/ds143-alexkutzke/material/raw/main/08/codes/abb.c);
