# Aula 02 - Revisão: tad, fila, lista e pilha

## Material

1. [Tipo abstrato de dados](02/00_tad/tad.pdf);
2. [Fila](02/01_fila/fila.pdf);
3. [Lista](02/02_lista/lista.pdf);
4. [Lista duplamente encadeada](02/02_lista/lista_dup.pdf);
5. [Pilha](02/03_pilha/pilha.pdf);
6. [Backtraking e pilhas: labirinto](02/03_pilha/labirinto.pdf);
