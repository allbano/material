# Aula 02 - Análise de Algoritmos e Union Find

## Análise de Algoritmos

* [Slides da vídeo-aula](02/analise_alg/analise_alg.pdf);

## Outros materiais

* [Sedgewick - Análise de algoritmos (em inglês)](https://algs4.cs.princeton.edu/14analysis/);
* [Sedgewick - Análise de algoritmos Slides (em inglês)](https://algs4.cs.princeton.edu/lectures/keynote/14AnalysisOfAlgorithms.pdf);
* [Nota de aula sobre notação e complexidade de algoritmos - Prof. Siang Wun Song (IME/USP)](https://www.ime.usp.br/~song/cursos/c1.pdf);
* [Sedgewick - Union-find (em inglês)](https://algs4.cs.princeton.edu/15uf/);
* [Sedgewick - Union-find Slides (em inglês)](https://algs4.cs.princeton.edu/lectures/keynote/15UnionFind.pdf);

## Implementações

* [Quick-Find](02/code/quick_find_uf.c);
* [Quick-Union](02/code/quick_union_uf.c);
* [Weighted Quick-Union](02/code/weighted_quick_union_uf.c);
