Tecnologia em Análise e Desenvolvimento de Sistemas

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*DS141 - Estruturas de Dados II*

Prof. Alexander Robert Kutzke

# Material da Disciplina DS141 - Estruturas de Dados II

## Tarefas

Nenhuma tarefa no momento.

## Trabalho prático

* [Indexador de textos (2018/2)](trabalho.md)

## Biblioteca de Grafos

* https://gitlab.com/ds143-alexkutzke/simple-c-graph-lib

## Lista de exercícios 

Nenhuma lista de exercícios no momento.

## Aulas

* [Aula 01 - Estudo de caso - Union-Find](aula_01.md);
* [Aula 02 - Análise de algoritmos e Union-Find](aula_02.md);
* [Aula 03 - Recursão e busca](aula_03.md);
* [Aula 04 - Implementação de tabela de símbolos](aula_04.md);
* [Aula 05 - Mergesort](aula_05.md);
* [Aula 06 - Quicksort](aula_06.md);
* [Aula 07 - Árvores e percursos](aula_07.md);
* [Aula 08 - Árvores de Busca Binária](aula_08.md);
* [Aula 09 - Árvores Balanceadas](aula_09.md);
* [Aula 10 - Tabelas Hash](aula_10.md);
* [Aula 11 - Grafos não Direcionados](aula_11.md);
* [Aula 12 - Grafos Direcionados](aula_12.md);
