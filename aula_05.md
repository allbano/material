# Aula 05 - Mergesort

## Material

1. [Merge Sort](https://gitlab.com/ds143-alexkutzke/material/-/blob/main/05/00_merge/mergesort.pdf);
2. [Implementações](https://gitlab.com/ds143-alexkutzke/material/-/tree/main/05/codes);
